`timecale 1ns / 1ps

module add_4_test();
	reg [3:0] A,B;
	reg clk;
	wire carry;
	wire [3:0] sum;
	initial
		begin
			A=0;
			B=0;
			clk=0;	
		end
	always #10 clk=~clk;
	always@(posedge clk)
		begin
			A={$random}%16;
			B={$random}%16;
		end
	add_4 adder(.A(A),.B(B),.carry(carry),.sum(sum));
endmodule