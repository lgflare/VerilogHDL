`timescale 1ns / 1ps

module add_1(
    input A,B,
    output sum,
    output carry
    );
    assign {carry,sum}=A+B;
endmodule