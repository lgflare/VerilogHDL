`timescale 1ns / 1ps


module nor_encoder(
    input Y_0,Y_1,Y_2,Y_3,Y_4,Y_5,Y_6,Y_7,
    output [2:0] Z
    );
    
   assign Z[2]=Y_4 || Y_5 || Y_6 || Y_7;
   assign Z[1]=Y_2 || Y_3 || Y_6 || Y_7;
   assign Z[0]=Y_1 || Y_3 || Y_5 || Y_7;
   
endmodule
