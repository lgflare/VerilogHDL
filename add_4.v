`timescale 1ns / 1ps

module add_4(
	input [3:0] A,B,
	output carry,
	output [3:0] sum
);
	assign {carry,sum}=A+B;
endmodule