`timescale 1ns / 1ps

module alert_test();
    reg R,Y,G;
    wire Z;
    
    initial
        begin
            R=0;Y=0;G=0;
            #20 G=1;
            #20 Y=1;
            #20 G=0;
            #20 R=1;
            #20 G=1;
            #20 Y=0;
            #20 G=0;
            #20 $stop;
        end
        
    alert Alert(.R(R),.Y(Y),.G(G),.Z(Z));
            
endmodule
