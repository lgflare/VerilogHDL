`timescale 1ns / 1ps

module add_test();
    reg A,B,clk;
   wire carry,sum;
    initial
        begin
            A=0;
            B=0;
            clk=0;
        end
    
    always #10 clk=~clk;
    always@(posedge clk)
        begin
            A={$random}%2;
            B={$random}%2;
        end
        
    add_1 adder(.A(A),.B(B),.sum(sum),.carry(carry));
    
endmodule
