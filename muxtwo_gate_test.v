`timescale 1ns / 1ps

module muxtwo_gate_test();
    reg a,b,sl,clk;
    wire out;
    
    muxtwo_gate MUXTWO(.a(a),.b(b),.sl(sl),.out(out));
    
    initial
        begin
            a=0;
            b=0;
            sl=0;
            clk=0;
        end
        
    always #10 clk=~clk;
    always #30 sl=~sl;
    always@(posedge clk)
        begin
            a={$random}%2;
            b={$random}%2;
        end
        
endmodule
