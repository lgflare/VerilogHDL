`timescale 1ns / 1ps

module alert(
    input R,Y,G,
    output Z
    );
    assign Z=(R && Y)||(R && G)||(Y && G)||((!R) && (!Y) && (!G));
endmodule
