`timescale 1ns / 1ps

module nor_encoder_test();
    reg Y_0,Y_1,Y_2,Y_3,Y_4,Y_5,Y_6,Y_7;
    wire [2:0] Z;
    
    initial
        begin
            Y_0=0;
            Y_1=0;
            Y_2=0;
            Y_3=0;
            Y_4=0;
            Y_5=0;
            Y_6=0;
            Y_7=0;
            #10 Y_0=1;
            #10 Y_0=0;Y_1=1;    //注意竞争-冒险现象，这样的赋值方式不可取
            #10 Y_1=0;Y_2=1;
            #10 Y_2=0;Y_3=1;
            #10 Y_3=0;Y_4=1;
            #10 Y_4=0;Y_5=1;
            #10 Y_5=0;Y_6=1;
            #10 Y_6=0;Y_7=1;
            #10 $stop;
        end
        
    nor_encoder Encoder(.Y_0(Y_0),.Y_1(Y_1),.Y_2(Y_2),.Y_3(Y_3),.Y_4(Y_4),.Y_5(Y_5),.Y_6(Y_6),.Y_7(Y_7),.Z(Z));
        
endmodule
