`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/28/2020 05:26:22 PM
// Design Name: 
// Module Name: select_1_test
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module select_1_test();
    reg A,B,clk,sl;
    wire result;
    initial
        begin
            A=0;
            B=0;
            clk=0;
            sl=0;
        end
        
    always #30 clk=~clk;
    always #50 sl=~sl;
    always@(posedge clk)
        begin
            A={$random}%2;
            B={$random}%2;
        end
        
    select_1 SL(.A(A),.B(B),.sl(sl),.result(result));
endmodule
